'''This program compares a file's MD5 hash with it's original - By Ahmed Sadman'''
# C O D E D  B Y  S A D M A N  M U H I B  S A M Y O

import wx, hashlib
from wx.lib.wordwrap import wordwrap

__metaclass__ = type
ID_ABOUTBOX = wx.NewId()

def check_md5(filename, block_size = 128):
	md5 = hashlib.md5()
	with open(filename, 'rb') as file:
		while True:
			data = file.read(block_size)
			if not data:break
			md5.update(data)
	if not file.closed: file.close()
	return md5.hexdigest().upper()
		
class FileDrop(wx.FileDropTarget):
	def __init__(self, window, field):
		super(FileDrop, self).__init__()
		self.window = window
		self.field = field
	
	def OnDropFiles(self, x, y, filenames):
		self.filepath = filenames[0]
		self.field.SetValue(self.filepath)
		
class MainGui(wx.Frame):
	def __init__(self, *args, **kwargs):
		super(MainGui, self).__init__(*args, **kwargs)
		self.InitUI()
		
	def InitUI(self):
		# creates the menu bar
		panel = wx.Panel(self)
		menubar = wx.MenuBar()
		file_menu = wx.Menu()
		
		file_menu.Append(ID_ABOUTBOX, 'About')
		file_menu.Append(wx.ID_EXIT)
		
		menubar.Append(file_menu, '&Menu')
		self.SetMenuBar(menubar)
		
		# ##### main layout #######
		
		vbox = wx.BoxSizer(wx.VERTICAL)
		hbox1 = wx.BoxSizer(wx.HORIZONTAL)
		hbox2 = wx.BoxSizer(wx.HORIZONTAL)
		
		# the user interaction area
		self.st1 = wx.StaticText(panel, label='File (Drag && Drop allowed)')
		self.st2 = wx.StaticText(panel, label='Original MD5')
		self.txt1 = wx.TextCtrl(panel)
		self.txt2 = wx.TextCtrl(panel)
		vbox.Add(self.st1, flag = wx.TOP | wx.LEFT | wx.RIGHT, border = 13)
		hbox1.Add(self.txt1, proportion = 1, flag = wx.EXPAND | wx.LEFT | wx.RIGHT, border =13)
		vbox.Add(hbox1, flag = wx.EXPAND)
		vbox.Add(self.st2, flag = wx.TOP | wx.LEFT | wx.RIGHT, border = 13)
		hbox2.Add(self.txt2, proportion = 1, flag = wx.EXPAND | wx.LEFT | wx.RIGHT, border =13)
		vbox.Add(hbox2, flag = wx.EXPAND)
		
		# output area
		hbox3 = wx.BoxSizer(wx.HORIZONTAL)
		hbox4 = wx.BoxSizer(wx.HORIZONTAL)
		hbox5 = wx.BoxSizer(wx.HORIZONTAL)
		self.waitString = 'Waiting for Input' # the string while waiting for input
		self.st3 = wx.StaticText(panel, label = '%-13s %-2s' % ('File\'s MD5', ':'))
		self.st3_output = wx.StaticText(panel, label = self.waitString)
		self.st4 = wx.StaticText(panel, label = '%-13s %-2s' % ('Original MD5', ':'))
		self.st4_output = wx.StaticText(panel, label = self.waitString)
		self.st5 = wx.StaticText(panel, label = '%-13s %-2s' % ('Status', ':'))
		self.st5_output = wx.StaticText(panel, label = self.waitString)
		
		font = wx.Font(pointSize = 10, family = wx.DEFAULT, faceName = "Courier New", style = wx.NORMAL, weight = wx.LIGHT)
		self.st3.SetFont(font)
		self.st3_output.SetFont(font)
		self.st4.SetFont(font)
		self.st4_output.SetFont(font)
		self.st5.SetFont(font)
		self.st5_output.SetFont(font)
		self.st5_output.SetForegroundColour('blue')
		self.st5.SetForegroundColour('blue')
		hbox3.Add(self.st3, flag = wx.LEFT, border = 15)
		hbox3.Add((0, 25))
		hbox3.Add(self.st3_output, flag = wx.RIGHT, border = 10)
		hbox4.Add(self.st4, flag = wx.LEFT, border = 15)
		hbox4.Add((0, 25))
		hbox4.Add(self.st4_output, flag = wx.RIGHT, border = 15)
		hbox5.Add(self.st5, flag = wx.LEFT, border = 15)
		hbox5.Add((0, 25))
		hbox5.Add(self.st5_output, flag = wx.RIGHT, border = 15)
		vbox.Add((20, 20))
		vbox.Add(hbox3, flag = wx.LEFT, border = 20)
		vbox.Add(hbox4, flag = wx.LEFT, border = 20)
		vbox.Add(hbox5, flag = wx.LEFT, border = 20)
		
		# places the buttons
		hbox6 = wx.BoxSizer(wx.HORIZONTAL)
		vbox.Add((-1, 10))
		self.btnCheck = wx.Button(panel, label='Check', size = (60, 40))
		self.btnQuit = wx.Button(panel, label='Quit', size = (60, 40))
		self.btnReset = wx.Button(panel, label='Reset', size = (60, 40))
		hbox6.Add(self.btnCheck, flag = wx.RIGHT, border = 1)
		hbox6.Add(self.btnReset, flag = wx.RIGHT, border = 1)
		hbox6.Add(self.btnQuit, flag = wx.RIGHT, border = 1)
		vbox.Add(hbox6, flag = wx.ALIGN_RIGHT | wx.ALL, border = 2)
		panel.SetSizer(vbox)
		
		dt = FileDrop(self.txt1, self.txt1)
		self.txt1.SetDropTarget(dt)
		
		# event bindings
		self.Bind(wx.EVT_MENU, self.OnQuit, id=wx.ID_EXIT)
		self.Bind(wx.EVT_BUTTON, self.OnQuit, self.btnQuit)
		self.Bind(wx.EVT_CLOSE, self.OnClose) # confirm before exiting
		self.Bind(wx.EVT_MENU, self.AboutBox, id=ID_ABOUTBOX)
		self.Bind(wx.EVT_BUTTON, self.ButtonReset, self.btnReset)
		self.Bind(wx.EVT_BUTTON, self.ButtonCheck, self.btnCheck)
		
		# app set-up
		self.SetTitle('Simple MD5 Checker')
		self.SetSize((470, 300))
		self.Centre()
		self.Show(True)
		
	def OnQuit(self, e):
		self.Close()
	
	def OnClose(self, e):
		dialog = wx.MessageDialog(None, 'Are you sure you want to quit?', 'Confirmation', wx.YES_NO | wx.NO_DEFAULT | wx.ICON_QUESTION)
		choice = dialog.ShowModal()
		
		if choice == wx.ID_YES: self.Destroy()
		else: e.Veto()
	
	def ButtonCheck(self, e):
		f = self.txt1.GetValue()
		if self.txt1.GetValue() and self.txt2.GetValue():
			try:
				md5 = check_md5(f)
				md5_original = self.txt2.GetValue().upper()
				self.st3_output.SetLabel(md5)
				self.st4_output.SetLabel(md5_original)
				if md5 == md5_original:
					self.st5.SetForegroundColour('#008b00')
					self.st5.Refresh()
					self.st5_output.SetLabel('Passed')
					self.st5_output.SetForegroundColour('#008b00')
					wx.MessageBox('File\'s MD5 matched successfully', 'Pass', wx.OK | wx.ICON_INFORMATION)
				else:
					self.st5.SetForegroundColour('red')
					self.st5.Refresh()
					self.st5_output.SetLabel('No Match Found')
					self.st5_output.SetForegroundColour('red')
			except IOError:
				wx.MessageBox('File not found', 'Error', wx.OK | wx.ICON_ERROR)
				pass
		else:
			wx.MessageBox('Please fill up all the fields', 'Required', wx.OK | wx.ICON_INFORMATION)
		
	def ButtonReset(self, e):
		self.txt1.SetValue('')
		self.txt2.SetValue('')
		self.st3_output.SetLabel(self.waitString)
		self.st4_output.SetLabel(self.waitString)
		self.st5_output.SetLabel(self.waitString)
		self.st5_output.SetForegroundColour('blue')
		self.st5.SetForegroundColour('blue')
		self.st5.Refresh()
		
	def AboutBox(self, e):
		description = wordwrap('The program gets a file as input and checks the file\'s MD5 with the original one. If MD5 checking is passed, then the file contents is good compared to the original. Otherwise, the file was modified from the original.\nWhat is MD5: It is one kind of record for your files, it\'s actually a string which represents the file contents. It gets changed when the file gets modified even the slightest', 300, wx.ClientDC(self))
		license = wordwrap('Completely Open - Source-free to remix, share or distribute', 300, wx.ClientDC(self))
		
		info = wx.AboutDialogInfo()
		info.SetName('Simple MD5 Checker')
		info.SetVersion('0.1')
		info.SetDescription(description)
		info.SetCopyright(' (C) Ahmed Sadman - 2014')
		info.SetLicense(license)
		info.AddDeveloper('Sadman Muhib Samyo')
		
		wx.AboutBox(info)
		
def main():
	app = wx.App()
	MainGui(None, style = wx.MINIMIZE_BOX | wx.SYSTEM_MENU | wx.CAPTION | wx.CLOSE_BOX)
	app.MainLoop()

if __name__ == '__main__':
	main()
