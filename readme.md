# Simple MD5 Checker #

A tool to verify MD5 checksum of a file

## License ##

Released under MIT License.

## Contributions ##

Any kind of contribution that will improve the program is always accepted.
